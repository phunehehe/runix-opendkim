{ config, home, lib, pkgs, ... }:

let

  inherit (config) domain;
  key-dir = "${home}/keys";
  key-file = "${key-dir}/${selector}.private";
  selector = "mail";
  user = "opendkim";

  config-file = pkgs.writeText "opendkim.conf" ''
    Socket   inet:${toString config.port}@localhost
    Domain   ${domain}
    KeyFile  ${key-file}
    Selector ${selector}
  '';

in {
  exports = {
    inherit (config) port;
  };
  options = {
    domain = lib.mkOption {
      type = lib.types.str;
    };
    port = lib.mkOption {
      type = lib.types.int;
      default = 8891;
    };
  };

  run = pkgs.writeBash "opendkim" ''

    PATH=${lib.makeBinPath [
      pkgs.coreutils
    ]}

    id ${user} || ${pkgs.shadow}/bin/useradd --system \
      --home /var/empty \
      ${user}

    if [[ ! -e "${key-file}" ]]
    then
      mkdir --parents ${key-dir}
      ${pkgs.opendkim}/bin/opendkim-genkey -s ${selector} -D ${key-dir} -d ${domain}
      chown ${user} ${key-file}
    fi

    exec ${pkgs.runit}/bin/chpst -u ${user} \
      ${pkgs.opendkim}/bin/opendkim -f -x ${config-file}
  '';
}
